import mergeDefault from "./lib/constant/DEFAULT_CONFIG";

export default mergeDefault({
  url: "https://events.example.com",
  data: {
    name: "Jon Doe", // used for default email footer
    email: "Jon Doe <jondoe@example.com>",
  },
});
