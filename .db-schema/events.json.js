export default {
  "8553906051c16700": {
    // random bytes ID
    _id: "8553906051c16700",
    // date of event creation (insertion into database)
    cdate: Date.now(),
    // event definition as provided by data/index.js
    definition: {
      // the event identifier (if not present anymore, the event gets cancelled)
      id: "event-id",
      // the title of the event (to be displayed in frontend)
      title: "Some Event",
      // the start date of the event
      date: "2018-05-19 15:00",
      // the (html) description of the event
      description: "some description html",
      // definition of invitation scheduling; see lib/constant/E_DELAY_TYPE.js for possible delay types
      access: [
        // immediate invitation of group with ID `0`
        {group: 0},
        // invitation of group with ID `1` when 50% of slots are taken
        {group: 1, delay: {type: "ENTRIES_RELATIVE", value: 0.5}},
        // invitation of group with ID `2` when 2 slots are taken
        {group: 2, delay: {type: "ENTRIES", value: 2}},
        // invitation of group with ID `3` after 24h (relative to event registration)
        {group: 3, delay: {type: "HOURS", value: 24}},
        // invitation of group with ID `3` at latest 24h before event start
        {group: 3, delay: {type: "HOURS", value: -24}},
      ],
      // event options
      options: {
        // forbid join/leave some time before the event start
        freezeTime: {
          // the amount of hours before the event start to forbid any more join requests; may be null
          join: 8,
          // the amount of hours before the event start to forbid any more leave requests; may be null
          leave: 8,
        },
        // the amount of hours after the event start to keep the event for reference; may be null
        keepOnline: 24,
        // the amount of slots required for the event to take place
        minEntries: 2,
        // the amount of available slots; may be null
        maxEntries: 8,
        // the exposure levels of event data; see lib/constant/E_EXPOSURE.js for possible values
        exposure: {
          // when to expose event at all (less exposure than ELIGIBLE makes no sense here)
          base: "INVITED",
          // when to expose display of event
          display: "YES",
          // when to expose amount of currently assigned slots
          entries: "YES",
          // when to expose whether one himself got assigned a slot assigned
          assigned: "YES",
          // when to expose the slot assignments
          slots: "CONFIRMED",
          // when to expose the queue entries (waiting for slot to become free)
          queue: "NO",
        },
        // email notification options; see lib/constant/E_NOTIFICATION_TYPE.js
        emails: {
          // group-based trigger
          invitation: true,
          // address-based status
          confirmation: true, success: true, dismiss: true,
          // event-based status
          fail: true,
          // admin triggered actions
          modification: true, cancel: true,
        },
      }
    },
    // the schedule is derived from the definition
    schedule: {
      close: Date.now(),
      stick: Date.now(),
      start: Date.now(),
      remove: Date.now(), // may be null
    },
  },
  // ...
};
