import path from "path";
import {readJSON} from "./lib/file";
import {prepare as prepareEvent} from "./lib/event";

export {addresses, groups, events};

/*==================================================== Constants  ====================================================*/

const BASE_PATH = __dirname;

/*==================================================== Functions  ====================================================*/

async function addresses() {
  const entries = await readJSON(path.join(BASE_PATH, "addresses", "entries.json"));
  const obj = {};
  entries.forEach((el) => obj[Buffer.from(el.email).toString("base64")] = el);
  return entries;
}

async function groups() {
  const groups = new Map();
  const array = await readJSON(path.join(BASE_PATH, "addresses", "groups.json"));
  array.forEach((group) => groups.set(group.id, group));
  return groups;
}

async function events() {
  const cache = {};
  const [events, accessGroups] = await Promise.all([
    readJSON(path.join(BASE_PATH, "events", "events.json")),
    readJSON(path.join(BASE_PATH, "events", "access.json")),
  ]);
  return await Promise.all(events.map(prepareEvent.bind(null, cache, accessGroups)));
}
