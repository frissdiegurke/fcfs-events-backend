import {entries as db} from "./_access";

export {db as access, get, add, remove};

/*==================================================== Functions  ====================================================*/

function get(eventId) {
  if (!db.data.hasOwnProperty(eventId)) { db.data[eventId] = []; }
  return db.data[eventId];
}

function add(eventId, addressId) {
  if (!db.data.hasOwnProperty(eventId)) { db.data[eventId] = []; }
  db.data[eventId].push(addressId);
  db.changed();
}

function remove(eventId, addressId) {
  if (!db.data.hasOwnProperty(eventId)) { return false; }
  if (addressId === void 0) {
    Reflect.deleteProperty(db.data, eventId);
  } else {
    const list = db.data[eventId];
    const idx = list.indexOf(addressId);
    if (idx === -1) { return false; }
    list.splice(idx, 1);
  }
  db.changed();
  return true;
}
