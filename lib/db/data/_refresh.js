import {refresh as refreshAdresses} from "./address";
import {refresh as refreshEvents} from "./event";
import {refresh as refreshGroups} from "./group";

export default async function () {
  await Promise.all([
    refreshEvents(),
    refreshAdresses()
      .then(refreshGroups),
  ]);
}
