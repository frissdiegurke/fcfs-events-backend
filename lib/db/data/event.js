import {getEvents} from "./_access";
import {log} from "../../logger";

export {refresh, getAll};

let eventList = null;

/*==================================================== Functions  ====================================================*/

async function refresh() {
  eventList = await (typeof getEvents === "function" ? getEvents() : getEvents);
  if (!Array.isArray(eventList)) { throw new Error("Invalid events definition."); }
  log.verbose("events read");
}

function getAll() { return eventList; }
