import {find, isEqual, merge, values} from "lodash";
import {randomBytes} from "crypto";

import {getAll as getAllDataEvents} from "./data/event";
import {difference} from "../util/misc";
import {log} from "../logger";
import {getDisplayExposedProperties, prepare} from "../services/event";
import {
  access as eventDB,
  add as addToDB,
  getAll as getDBObj,
  remove as removeFromDB,
  update as updateDB
} from "./fs/event";
import {
  created as triggerCreated,
  loaded as triggerLoaded,
  modified as triggerModified,
  removed as triggerRemoved
} from "../services/notification/trigger";

export default async function mergeDataIntoDB() {
  const dataArr = getAllDataEvents();
  const dbObj = getDBObj(), dbArr = values(dbObj);
  const dataEventIDCheck = {};
  let writeDB = false;
  await Promise.all(dataArr.map(prepare).map(checkDataEvent));
  await Promise.all(dbArr.map(checkDBEvent));
  if (writeDB) { await eventDB.changed().write(); }

  async function checkDataEvent(dataEvent) {
    const dbEvent = find(dbArr, (dbEvent) => dbEvent.definition.id === dataEvent.definition.id);
    if (dbEvent == null) {
      dataEvent._id = randomBytes(8).toString("hex");
      addToDB(dataEvent);
      triggerCreated(dataEvent);
      writeDB = true;
      log.verbose("event got added", {event: dataEvent});
    } else {
      dataEventIDCheck[dataEvent._id = dbEvent._id] = true;
      const eventProp = getDisplayExposedProperties(dataEvent), dbEventProp = getDisplayExposedProperties(dbEvent);
      if (!isEqual(eventProp, dbEventProp)) {
        dataEvent.cdate = dbEvent.cdate; // must not change
        const diff = difference(dbEventProp, eventProp);
        log.verbose("event got modified", {id: dataEvent.definition.id, eventProp, dbEventProp, diff});
        updateDB(merge(dbEvent, dataEvent));
        triggerModified(dbEvent, diff);
        writeDB = true;
      }
      triggerLoaded(dataEvent);
    }
  }

  async function checkDBEvent(dbEvent) {
    if (dataEventIDCheck[dbEvent._id] !== true) {
      removeFromDB(dbEvent._id);
      triggerRemoved(dbEvent);
      writeDB = true;
    }
  }
}
