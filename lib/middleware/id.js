let requestID = 0;

export default function (ctx, next) {
  ctx.request.id = requestID++;
  return next();
}
