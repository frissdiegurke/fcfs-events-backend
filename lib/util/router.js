import {log} from "../logger";

export {errorHandler, timeout};

/*==================================================== Functions  ====================================================*/

async function errorHandler(ctx, next) {
  try {
    return await next();
  } catch (err) {
    if (!(err instanceof Error) && typeof err.status === "number" && err.expose !== false) {
      ctx.status = err.status;
      if (err.message) { ctx.body = {message: err.message}; }
      log.debug("request failed", {err});
    } else {
      ctx.status = 500;
      log.error("request failed", {err});
    }
  }
}

function timeout(ms) {
  return async (ctx, next) => {
    ctx.timeout = ms;
    let timeout = null;
    const timeoutPromise = new Promise((resolve, reject) => {
      timeout = setTimeout(done, ms);

      function done() {
        timeout = null;
        if (ctx.timeout === false) { return; }
        if (ctx.timeout > ms) { return timeout = setTimeout(done, ctx.timeout - ms); }
        reject({status: 504});
      }
    });
    const reqPromise = next().finally(() => { timeout != null && clearTimeout(timeout); });
    return await Promise.race([reqPromise, timeoutPromise]);
  };
}
