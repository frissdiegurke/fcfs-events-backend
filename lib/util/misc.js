import {each} from "lodash";

export {mapAsync, mapDeep, difference};

/*==================================================== Functions  ====================================================*/

async function mapAsync(obj, iteratee) {
  if (typeof obj !== "object" || obj == null) { return obj; }
  if (obj instanceof Array) { return await Promise.all(obj.map(iteratee)); }
  const promises = [];
  const result = {};
  for (let key in obj) {
    if (obj.hasOwnProperty(key)) {
      const promise = iteratee(obj[key], key);
      if (promise != null && typeof promise.then === "function") {
        promises.push(promise.then((value) => result[key] = value));
      } else {
        result[key] = promise;
      }
    }
  }
  return await Promise.all(promises).then(() => result);
}

function mapDeep(obj, iteratee) {
  if (typeof obj !== "object" || obj == null) { return obj; }
  if (obj instanceof Array) {
    const result = [];
    for (let i = 0; i < obj.length; i++) {
      const value = obj[i];
      result.push(typeof value === "object" && value !== null ? mapDeep(value, iteratee) : iteratee(value));
    }
    return result;
  } else {
    const result = {};
    for (let key in obj) {
      if (obj.hasOwnProperty(key)) {
        const value = obj[key];
        result[key] = typeof value === "object" && value !== null ? mapDeep(value, iteratee) : iteratee(value);
      }
    }
    return result;
  }
}

function difference(from, to, prefix = null, target = []) {
  each(from, (value, key) => {
    if (typeof value === "object" && value !== null) {
      if (typeof to[key] === "object" && to[key] !== null) {
        return difference(value, to[key], prefix ? prefix + "." + key : key, target);
      }
    }
    if (value !== to[key]) { target.push([prefix ? prefix + "." + key : key, value, to[key]]); }
  });
  each(to, (value, key) => {
    if (!from.hasOwnProperty(key)) { target.push([prefix ? prefix + "." + key : key, void 0, value]); }
  });
  return target;
}
