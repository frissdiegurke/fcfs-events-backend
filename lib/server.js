import Koa from "koa";
import Router from "koa-router";

import config from "../config";
import {log} from "./logger";

import attachId from "./middleware/id";
import trackTiming from "./middleware/timing";
import logging from "./middleware/logger";
import {errorHandler, timeout} from "./util/router";

import mountEventPage from "./routes/event";

/*==================================================== Functions  ====================================================*/

export default function start() {
  const app = new Koa();
  const eventRouter = new Router();

  app
    .use(timeout(5000))
    .use(errorHandler)
    .use(attachId)
    .use(trackTiming)
    .use(logging)
    .use(eventRouter.routes(), eventRouter.allowedMethods());

  mountEventPage(eventRouter);

  app.listen(config.port);

  log.info("server started", {config});
}
