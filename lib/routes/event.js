import bodyParser from "koa-bodyparser";
import config from "../../config";
import {access as entryDB, add as addEntry, get as getEntries, remove as removeEntry} from "../db/fs/entry";
import {access as notificationDB, removeAll as removeNotifications} from "../db/fs/notification";
import {updated as eventUpdated} from "../services/notification/trigger";
import {getDisplayExposedProperties, getEventStatus, getExposure, getSelfStatus} from "../services/event";
import {resolve as resolveAddressId} from "../db/fs/token";
import {byId as eventById} from "../db/fs/event";
import NOTIFICATION_TYPE from "../constant/E_NOTIFICATION_TYPE";
import {byId as addressById} from "../db/data/address";

export default function mount(router) {
  const bodyMW = bodyParser();

  router.get("/:eventId", middleware, routeEvent);
  router.post("/:eventId/join", bodyMW, middleware, routeJoin);
  router.post("/:eventId/leave", bodyMW, middleware, routeLeave);
}

/*====================================================== Routes ======================================================*/

async function routeEvent(ctx) {
  if (!ctx.eventExposure.display) { return Promise.reject({status: 403, message: "Event display denied."}); }
  ctx.body = {
    event: getDisplayExposedProperties(ctx.event),
    status: {self: getExposedSelfStatus(ctx), event: getExposedEventStatus(ctx)},
  };
}

async function routeJoin(ctx) {
  const status = ctx.eventStatus;
  if (!status.self.invited) { return Promise.reject({status: 401, message: "Not invited to event."}); }
  if (status.self.joined) { return Promise.reject({status: 403, message: "Already participating in this event."}); }
  if (status.event.closed) { return Promise.reject({status: 403, message: "Event is closed."}); }
  addEntry(ctx.event._id, ctx.addressId);
  eventUpdated(ctx.event);
  await Promise.all([entryDB.write(), notificationDB.write()]);
  ctx.redirect("back", config.url + "/event/" + ctx.event._id + "?" + ctx.token);
}

async function routeLeave(ctx) {
  const status = ctx.eventStatus;
  if (!status.self.joined) { return Promise.reject({status: 403, message: "Not participating in this event"}); }
  if (status.event.sticky) {
    return Promise.reject({status: 403, message: "Leaving this event is not allowed anymore."});
  }
  removeEntry(ctx.event._id, ctx.addressId);
  removeNotifications(ctx.event._id, {address: ctx.addressId, type: NOTIFICATION_TYPE.ADDRESS.CONFIRMATION});
  eventUpdated(ctx.event);
  await Promise.all([entryDB.write(), notificationDB.write()]);
  ctx.redirect("back", config.url + "/event/" + ctx.event._id + "?" + ctx.token);
}

/*==================================================== Middleware ====================================================*/

async function middleware(ctx, next) {
  const token = ctx.query.token || ctx.request.body && ctx.request.body.token;
  const event = eventById(ctx.params.eventId);
  if (event == null) { return Promise.reject({status: 404}); }
  const entries = getEntries(event._id);
  const addressId = event && resolveAddressId(event._id, token);
  const addressIndex = addressId == null ? -1 : entries.indexOf(addressId);

  const eventStatus = {
    event: getEventStatus(event, entries.length, ctx.request.startTime),
    self: getSelfStatus(event, addressId, addressIndex),
  };
  const eventExposure = getExposure(event, eventStatus);

  if (!eventExposure.base) { return Promise.reject({status: 403, message: "Event access denied."}); }

  ctx.token = token;
  ctx.addressId = addressId;
  ctx.event = event;
  ctx.eventEntries = entries;
  ctx.eventStatus = eventStatus;
  ctx.eventExposure = eventExposure;

  return await next();
}

/*======================================================= Misc =======================================================*/

function getExposedSelfStatus(ctx) {
  const exposure = ctx.eventExposure, status = ctx.eventStatus.self;
  return {
    invited: status.invited,
    joined: status.joined,
    assigned: exposure.assigned ? status.assigned : null,
  };
}

function getExposedEventStatus(ctx) {
  const exposure = ctx.eventExposure, status = ctx.eventStatus.event;
  const maxEntries = ctx.event.definition.options.maxEntries, entries = ctx.eventEntries;
  let slots = null, queue = null;
  if (exposure.slots) {
    slots = maxEntries != null ? entries.slice(0, maxEntries).map(getExposedAddress.bind(null, ctx)) : entries;
  }
  if (exposure.queue) {
    queue = maxEntries != null ? entries.slice(maxEntries).map(getExposedAddress.bind(null, ctx)) : [];
  }
  return {
    sufficed: (exposure.entries || exposure.slots) ? status.sufficed : null,
    closed: status.closed,
    sticky: status.sticky,
    entries: exposure.entries ? entries.length : null,
    slots,
    queue,
  };
}

function getExposedAddress(ctx, addressId) {
  return {name: addressById(addressId).name, self: addressId === ctx.addressId};
}
