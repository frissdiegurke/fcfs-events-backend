const MAX_INT32 = 0x7FFFFFFF;

const TIME_TRIGGERED = {/* groupId: [{cancel:Function}] */};
const CONDITION_TRIGGERED = {/* groupId: [{cancel:Function,run:Function}] */};

export {delay, cancelAll, when, checkWhens};

function cancelAll(groupId) {
  cancelTimeouts(groupId);
  cancelWhens(groupId);
}

function cancelTimeouts(groupId) {
  if (!TIME_TRIGGERED.hasOwnProperty(groupId)) { return; }
  cancelArray(TIME_TRIGGERED[groupId]);
  Reflect.deleteProperty(TIME_TRIGGERED, groupId);
}

function cancelWhens(groupId) {
  if (!CONDITION_TRIGGERED.hasOwnProperty(groupId)) { return; }
  cancelArray(CONDITION_TRIGGERED[groupId]);
  Reflect.deleteProperty(CONDITION_TRIGGERED, groupId);
}

function delay(groupId, msDelay, cb) {
  if (!TIME_TRIGGERED.hasOwnProperty(groupId)) { TIME_TRIGGERED[groupId] = []; }
  const list = TIME_TRIGGERED[groupId];
  if (msDelay <= 0) {
    const cancellable = makeCancellable(cb);
    list.push(cancellable);
    process.nextTick(() => cancellable.run());
  } else {
    let timeoutId;
    // setTimeout limit is MAX_INT32
    if (msDelay > MAX_INT32) {
      timeoutId = setTimeout(() => delay(groupId, msDelay, cb), MAX_INT32);
    } else {
      timeoutId = setTimeout(cb, msDelay);
    }
    list.push({cancel: () => clearTimeout(timeoutId)});
  }
}

function when(groupId, cb, conditionCb) {
  if (!CONDITION_TRIGGERED.hasOwnProperty(groupId)) { CONDITION_TRIGGERED[groupId] = []; }
  const cancellable = makeCancellable(cb, conditionCb);
  CONDITION_TRIGGERED[groupId].push(cancellable);
  process.nextTick(() => cancellable.run());
}

function checkWhens(groupId) {
  if (!CONDITION_TRIGGERED.hasOwnProperty(groupId)) { return; }
  const list = CONDITION_TRIGGERED[groupId];
  for (let i = 0; i < list.length; i++) { list[i].run(); }
}

function makeCancellable(cb, conditionCb) {
  return {
    cancelled: false,

    run() {
      if (this.cancelled || typeof conditionCb === "function" && !conditionCb()) { return; }
      this.cancel();
      cb();
    },
    cancel() { this.cancelled = true; }
  };
}

function cancelArray(array) { for (let i = 0; i < array.length; i++) { array[i].cancel(); } }
