import nodemailer from "nodemailer";
import {template} from "lodash";

import config from "../../config";
import {log} from "../logger";

export {sendMail};

const cfg = config.mail;
const from = template(cfg.from)({config});
const subjectT = template(cfg.subject || "<%= subject %>");
const textTpl = Array.isArray(cfg.text) ? cfg.text.join("\n") : cfg.text;
if (typeof textTpl !== "string") { throw new Error("mail.text configuration has invalid type."); }
const htmlTpl = Array.isArray(cfg.html) ? cfg.html.join("<br/>") : cfg.html;
const textT = template(textTpl);
const htmlT = htmlTpl && template(htmlTpl);

const transport = nodemailer.createTransport({sendmail: true});

/*==================================================== Functions  ====================================================*/

function sendMail(address, data) {
  data.contact = address;
  return new Promise((resolve, reject) => {
    const mailData = {
      from,
      to: address.email,
      subject: subjectT(data),
      text: textT(data),
    };
    if (typeof htmlT === "function") { mailData.html = htmlT(data); }
    if (config.mail.mute) {
      log.debug("mail muted", {mail: mailData});
      resolve();
    } else {
      log.debug("sending mail", {mail: mailData});
      transport.sendMail(mailData, (err) => err == null ? resolve() : reject(err));
    }
  });
}
