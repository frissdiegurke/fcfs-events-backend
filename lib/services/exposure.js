import EXPOSURE, {check as checkExposure} from "../constant/E_EXPOSURE";

// eslint-disable-next-line complexity
export function isExposed(exposure, status) {
  const type = checkExposure(exposure);
  // check base cases
  if (type === EXPOSURE.NO) { return false; }
  if (type === EXPOSURE.YES) { return true; }
  // check invitation
  if (!status.self.invited) { return false; }
  if (type === EXPOSURE.INVITED) { return true; }
  // check eligibility
  if (status.event.closed && !status.self.joined) { return false; }
  if (type === EXPOSURE.ELIGIBLE) { return true; }
  // check within slots/queue
  if (!status.self.joined) { return false; }
  if (type === EXPOSURE.QUEUE) { return true; }
  // check within slots
  if (!status.self.assigned) { return false; }
  if (type === EXPOSURE.SLOT) { return true; }
  // check not able to leave
  if (!status.event.sticky) { return false; }
  if (type === EXPOSURE.STUCK) { return true; }
  // check event sufficed
  if (!status.event.sufficed) { return false; }
  if (type === EXPOSURE.CONFIRMED) { return true; }
  // check slots fixed
  if (!status.event.closed) { return false; }
  if (type === EXPOSURE.FIXED) { return true; }
  // noinspection RedundantIfStatementJS
  return false;
}
