import DELAY_TYPE, {exists as typeExists, getMilliseconds, isTimeTriggered} from "../../constant/E_DELAY_TYPE";
import {cancelAll, checkWhens, delay, when} from "../delay";
import {get as getEntries} from "../../db/fs/entry";

const WHEN_TYPES = {
  entries(event, value) { return getEntries(event._id).length >= value; },
  entriesRelative(event, value) {
    const maxEntries = event.definition.options.maxEntries;
    if (maxEntries == null) { throw new Error("Relative entries not possible for unset max entries."); }
    return getEntries(event._id).length / maxEntries >= value;
  },
};

export {
  initEvent, clearEvent, checkEvent,
  dispatchRemoval, dispatchFreezeJoin, dispatchFreezeLeave, dispatchInvitations,
};

/*==================================================== Functions  ====================================================*/

function initEvent(event) { clearEvent(event); }

function clearEvent(event) { cancelAll(event._id); }

function checkEvent(event) { checkWhens([event._id]); }

/*------------------------------------------------- Event dispatcher -------------------------------------------------*/

function dispatchRemoval(event, cb) {
  if (event.schedule.remove == null) { return false; }
  delay(event._id, event.schedule.remove - Date.now(), cb);
}

function dispatchFreezeJoin(event, cb) { delay(event._id, event.schedule.close - Date.now(), cb); }

function dispatchFreezeLeave(event, cb) { delay(event._id, event.schedule.stick - Date.now(), cb); }

function dispatchInvitations(event, cb) {
  const _event = event.definition;
  if (!_event.options.emails.invitation || _event.access == null) { return; }
  const access = _event.access instanceof Array ? _event.access : [_event.access];
  access.forEach((access) => { genericDelay(event, access.delay, () => cb(event, access.group)); });
}

function genericDelay(event, delayValue, cb) {
  if (typeof delayValue === "number") {
    timeDelay(event, {value: delayValue}, cb);
  } else if (typeof delayValue !== "object" || delayValue === null) {
    timeDelay(event, {value: 0, type: DELAY_TYPE.MILLISECONDS, from: event.schedule.cdate}, cb);
  } else if (!typeExists(delayValue.type)) {
    throw new Error("Delay type does not exist: " + delayValue.type);
  } else if (isTimeTriggered(delayValue.type)) {
    timeDelay(event, delayValue, cb);
  } else {
    conditionDelay(event, delayValue.value, delayValue.type, cb);
  }
}

function timeDelay(event, data, cb) {
  const msDelay = getMilliseconds(data.value, data.type);
  const from = data.from == null ? (msDelay < 0 ? event.schedule.start : event.cdate) : data.from;
  delay(event._id, from + msDelay - Date.now(), cb);
}

function conditionDelay(event, value, type, cb) {
  switch (DELAY_TYPE[type]) {
    case DELAY_TYPE.ENTRIES:
      return when(event._id, cb, WHEN_TYPES.entries.bind(null, event, value));
    case DELAY_TYPE.ENTRIES_RELATIVE:
      return when(event._id, cb, WHEN_TYPES.entriesRelative.bind(null, event, value));
    default:
      throw new Error("Delay type '" + type + "' not supported yet.");
  }
}
