import {access as notificationDB, removeAll as removeNotifications} from "../../db/fs/notification";
import {access as tokenDB, remove as removeTokens} from "../../db/fs/token";
import {access as entryDB, get as getEntries, remove as removeEntries} from "../../db/fs/entry";
import {access as eventDB, remove as removeEvent} from "../../db/fs/event";
import {
  cancel as sendCancel,
  confirm as sendConfirm,
  fail as sendFail,
  invitations as sendInvitations,
  modification as sendModification,
  success as sendSuccess,
} from "./send";
import * as dispatcher from "./dispatch";
import {getEventStatus} from "../event";
import {log} from "../../logger";

export {created, loaded, modified, updated, removed};

/*==================================================== Functions  ====================================================*/

/*-------------------------------------------------- Event trigger  --------------------------------------------------*/

function created(event) { return loaded(event); }

function updated(event) {
  dispatcher.checkEvent(event);
  wrapCheck(event, checkAll)();
}

function loaded(event) {
  dispatcher.initEvent(event);
  dispatcher.dispatchInvitations(event, (event, groupId) => sendInvitations(event, groupId).catch(log.err));
  dispatcher.dispatchFreezeJoin(event, wrapCheck(event, checkEventFailed));
  dispatcher.dispatchFreezeLeave(event, wrapCheck(event, checkEventSucceeded));
  dispatcher.dispatchRemoval(event, wrapCheck(event, checkEventRemoved));
}

function modified(event, diff) {
  const eventOptions = event.definition.options;
  if (eventOptions.emails.modification) {
    sendModification(event, diff).catch(log.err);
  }
}

function removed(event) { wrapCheck(event, checkEventRemoved)(); }

/*------------------------------------------------- Event conditions -------------------------------------------------*/

async function checkAll(event, entries, eventStatus) {
  await Promise.all([
    checkSlotAssigned(event, entries, eventStatus),
    checkEventFailed(event, entries, eventStatus),
    checkEventSucceeded(event, entries, eventStatus),
    checkEventRemoved(event, entries, eventStatus),
  ])
    .catch(log.err);
}

async function checkSlotAssigned(event, entries) {
  const eventOptions = event.definition.options;
  await Promise
    .all([
      eventOptions.emails.confirmation && sendConfirm(event, entries.slice(0, eventOptions.maxEntries)),
    ])
    .catch(log.err);
}

async function checkEventFailed(event, entries, eventStatus) {
  if (eventStatus.sufficed || !eventStatus.closed) { return; }
  const eventOptions = event.definition.options;
  await Promise
    .all([
      eventOptions.emails.fail && sendFail(event, entries),
    ])
    .catch(log.err);
}

async function checkEventSucceeded(event, entries, eventStatus) {
  if (!eventStatus.sufficed || !eventStatus.sticky) { return; }
  const eventOptions = event.definition.options;
  await Promise
    .all([
      eventOptions.emails.success && sendSuccess(event, entries.slice(0, eventOptions.maxEntries)),
      eventOptions.emails.dismiss && sendSuccess(event, entries.slice(eventOptions.maxEntries)),
    ])
    .catch(log.err);
}

async function checkEventRemoved(event, entries, eventStatus) {
  if (!eventStatus.removed) { return; }
  const eventOptions = event.definition.options;
  dispatcher.clearEvent(event);
  eventOptions.emails.cancel && await sendCancel(event, entries);
  await Promise
    .all([
      removeEntries(event._id) && entryDB.write(),
      removeNotifications(event._id) && notificationDB.write(),
      removeTokens(event._id) && tokenDB.write(),
      removeEvent(event._id) && eventDB.write(),
    ])
    .catch(log.err);
}

/*------------------------------------------------- Event conditions -------------------------------------------------*/

function wrapCheck(event, cb) {
  return () => {
    const entries = getEntries(event._id);
    const eventStatus = getEventStatus(event, entries.length, Date.now());
    cb(event, entries, eventStatus).catch(log.err);
  };
}
