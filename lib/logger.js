import { createLogger } from "oddlog";

export const log = addAliases(createLogger("app", {
  transports: [
    {type: "stream", level: "trace"}
  ]
}));

function addAliases(logger) {
  logger.err = (err) => { logger.error({err}); };
  logger.tErr = (err) => {
    logger.error({err});
    throw err;
  };
  const _child = logger.child;
  logger.child = (...args) => addAliases(Reflect.apply(_child, logger, args));
  return logger;
}
