import path from "path";
import {defaultsDeep} from "lodash";

export default function (options) {
  const result = defaultsDeep(
    options,
    {
      port: process.env.PORT || 3001,
      url: process.env.BASE_URL || "http://localhost:3000",
      translations: "en",
      data: {},
      mail: {
        mute: process.env.MUTE_MAILS === "true",
        from: "<%= config.data.email %>",
        subject: "<%= subject %> | FCFS-Events",
        text: [
          "Hello <%= data.address.name %>.",
          "",
          "<%= content.text %>",
          "",
          "Best regards",
          "<%= config.data.name %>",
        ],
        html: [
          "<b>Hello <%= data.address.name %>.</b>",
          "",
          "<%= content.html %>",
          "Best regards",
          "<emph><%= config.data.name %></emph>",
        ],
      },
      paths: {
        base: path.join(__dirname, "..", ".."),
        db: "data/.db",
      },
    }
  );

  if (!options.data.hasOwnProperty("name")) {
    Reflect.defineProperty(options.data, "name", {get() { throw new Error("data.name config not set."); }});
  }
  if (!options.data.hasOwnProperty("email")) {
    Reflect.defineProperty(options.data, "email", {get() { throw new Error("data.email config not set."); }});
  }

  return result;
}
