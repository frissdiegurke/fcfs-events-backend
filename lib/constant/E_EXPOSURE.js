import {invert} from "lodash";

const VALUES = {
  NO: 0,
  FIXED: 1, // ADDRESS is assigned a slot and assignments are fixed (no join/leave allowed anymore).
  CONFIRMED: 2, // ADDRESS is assigned a slot, may not leave anymore and EVENT has sufficient entries
  STUCK: 3, // ADDRESS is assigned a slot and may not leave anymore
  SLOT: 4, // ADDRESS is assigned a slot
  QUEUE: 5, // ADDRESS is assigned a slot or within the queue
  ELIGIBLE: 6, // ADDRESS is assigned a slot or within the queue or is allowed to join
  INVITED: 7, // ADDRESS is invited
  YES: 8,
};

export default VALUES;
export const INVERSE = invert(VALUES);

export function check(type) {
  if (typeof type === "string") {
    if (!VALUES.hasOwnProperty(type)) { throw new Error("Exposure type invalid: " + type); }
    type = VALUES[type];
  }
  if (typeof type !== "number" || !INVERSE.hasOwnProperty(type)) { throw new Error("Exposure type invalid: " + type); }
  return type;
}
