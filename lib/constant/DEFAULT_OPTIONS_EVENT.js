import EXPOSURE from "../constant/E_EXPOSURE";

export default {
  keepOnline: 24, // in hours after the event started
  minEntries: 0,
  maxEntries: null,
  freezeTime: { // in hours before the event starts
    join: 8,
    leave: 8,
  },
  exposure: {
    base: EXPOSURE.INVITED, // the base level required for everything else
    display: EXPOSURE.YES,
    entries: EXPOSURE.YES,
    assigned: EXPOSURE.YES,
    slots: EXPOSURE.CONFIRMED,
    queue: EXPOSURE.NO,
  },
  emails: {
    invitation: true,
    confirmation: true,
    success: true,
    dismiss: true,
    fail: true,
    modification: true,
    cancel: true,
  },
};
